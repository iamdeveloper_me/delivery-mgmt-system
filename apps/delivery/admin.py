from django.contrib import admin
from apps.delivery.models import *

# Register your models here.
admin.site.register(Order)
admin.site.register(Transport)
admin.site.register(Feedback)
