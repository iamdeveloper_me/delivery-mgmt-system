from django.db import models

from django.db import models

class Transport(models.Model):
    delivery_choice = (
        (1,'Pickup Delivery'),
        (2,'Door Delivery'),
    )
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    contact = models.CharField(max_length=15, null=True)
    email = models.EmailField(max_length=70, unique=True)
    verification_code = models.CharField(max_length=4)
    status = models.IntegerField(default=1, choices=delivery_choice)

    def __str__ (self):
        return '%s, %s, %s, %s, %s, %s' % (self.name,self.address,self.contact,self.email,self.verification_code,self.status)

class Order(models.Model):
    delivery_status = (
        (1,'Pending'),
        (2,'Delivered'),
    )
    shop_name = models.CharField(max_length=255)
    product_name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    contact = models.CharField(max_length=15)
    email = models.EmailField(max_length=70, unique=True)
    delivery_date = models.DateField(auto_now=False, auto_now_add=False)
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(default=1, choices=delivery_status) 
    transport = models.ForeignKey(Transport, on_delete=models.CASCADE)

    def __str__ (self):
        return '%s, %s, %s, %s, %s, %s, %s, %s, %s' % (self.shop_name, self.product_name, self.address, self.contact, self.email, self.delivery_date, self.status, self.created_at, self.transport)

class Feedback(models.Model):
    Rating_CHOICES = (
        (1, 'Poor'),
        (2, 'Average'),
        (3, 'Good'),
        (4, 'Very Good'),
        (5, 'Excellent')
    )

    rating = models.IntegerField(choices=Rating_CHOICES, default=1)
    status = models.CharField(max_length=255)
    transport = models.ForeignKey(Transport, on_delete=models.CASCADE)

    def __str__ (self):
        return '%s, %s, %s' % (self.rating, self.status, self.transport)


